# chat/routing.py
from django.urls import re_path

from . import consumers

websocket_urlpatterns = [
    re_path("ws/canvas/", consumers.CanvasConsumer.as_asgi()),
    re_path("ws/graph/", consumers.GraphConsumer.as_asgi()),
    re_path("ws/com/", consumers.ComConsumer.as_asgi()),
]